app = angular.module("pokerApp");
app.directive("pokerSubmitDirective", function() {

    return {
        restrict : "AE",
        scope : true,
        templateUrl : '/submitform/submitform.html',
        controller: 'mainController'
    };
});